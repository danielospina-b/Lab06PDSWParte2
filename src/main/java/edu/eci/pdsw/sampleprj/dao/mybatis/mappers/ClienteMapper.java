package edu.eci.pdsw.sampleprj.dao.mybatis.mappers;

import edu.eci.pdsw.samples.entities.Cliente;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author 2106913
 */
public interface ClienteMapper {
    
    public Cliente consultarCliente(@Param("idcli") int id); 
    
    /**
     * Registrar un nuevo item rentado asociado al cliente identificado
     * con 'idc' y relacionado con el item identificado con 'idi'
     * @param id documento del cliente
     * @param idit id del item a registrar
     * @param idir id del item rentado
     * @param fechainicio ...
     * @param fechafin ...
     */
    public void agregarItemRentadoACliente(@Param("idcli") int id, 
            @Param("iditem") int idit,
            @Param("idir") int idir,
            @Param("finicio") Date fechainicio,
            @Param("ffin") Date fechafin);

    /**
     * Consultar todos los clientes
     * @return 
     */
    public List<Cliente> consultarClientes();
    
}
